package uk.ac.glasgow.dcs.qualitypolice.rest;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import uk.ac.glasgow.dcs.qualitypolice.impl.TicketUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/age")
public class TicketAgeResource {
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTicketsByAge(@QueryParam("project") String project,
                                    @QueryParam("user") String user,
                                    @QueryParam("incResolved") boolean incResolved,
                                    @QueryParam("incCurrent") boolean incCurrent) {
        List<MutableIssue> allIssues = TicketUtils.getTicketsFromQuery(project, user, incResolved, incCurrent);
        Map<String, Integer> issueAge = new HashMap<String, Integer>();
        for (MutableIssue issue : allIssues)
            issueAge.put(issue.getKey(), TicketUtils.getTicketAge(issue));
        JSONObject json = new JSONObject();
        try {
            List<Integer> values = new ArrayList<Integer>(issueAge.values());
            Collections.sort(values);
            for (Integer i : values)
                json.put(Integer.toString(i), new JSONArray());
            for (String key : issueAge.keySet())
                json.getJSONArray(Integer.toString(issueAge.get(key))).put(key);
        } catch (Exception ignored) {
        }
        return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
    }

}
