package uk.ac.glasgow.dcs.qualitypolice.impl;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.ofbiz.core.entity.GenericEntityException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Class of static methods
 * Makes previously inconvenient ticket DB queries easier
 */
public class TicketUtils {
    private static ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private static UserManager userManager = ComponentAccessor.getUserManager();
    private static IssueManager issueManager = ComponentAccessor.getIssueManager();
    private static CommentManager commentManager = ComponentAccessor.getCommentManager();
    private static ChangeHistoryManager changeHistoryManager = ComponentAccessor.getChangeHistoryManager();

    public static Collection<Long> getAllIssueIds() {
        List<Long> allIssueIds = new ArrayList<Long>();
        for (Project project : projectManager.getProjectObjects()) {
            try {
                Collection<Long> issueIds = issueManager.getIssueIdsForProject(project.getId());
                for (Long issueId : issueIds)
                    allIssueIds.add(issueId);
            } catch (GenericEntityException e) {
                // non-fatal & ignored, should not happen
            }
        }
        return allIssueIds;
    }

    public static List<MutableIssue> getTicketsFromQuery(String projectKey,
                                                         String userName,
                                                         boolean incResolved,
                                                         boolean incCurrent) {
        Project project = null;
        ApplicationUser user = null;
        if (userName != null)
            user = userManager.getUserByName(userName);
        if (projectKey != null)
            project = projectManager.getProjectObjByKey(projectKey);
        List<MutableIssue> allIssues = new ArrayList<MutableIssue>();
        try {
            Collection<Long> issueIds;
            if (project != null)
                issueIds = issueManager.getIssueIdsForProject(project.getId());
            else
                issueIds = getAllIssueIds();
            for (Long issueId : issueIds) {
                MutableIssue issue = issueManager.getIssueObject(issueId);
                Timestamp resolution = issue.getResolutionDate();
                if ((user == null || user.equals(issue.getReporter())) &&
                        ((incResolved && resolution != null) || (incCurrent && resolution == null)))
                    allIssues.add(issue);
            }
        } catch (GenericEntityException e) {
            // non-fatal & ignored, should not happen
        }
        return allIssues;
    }

    public static List<ChangeHistory> getTicketChanges(MutableIssue issue) {
        return changeHistoryManager.getChangeHistories(issue);
    }

    public static List<Comment> getTicketComments(MutableIssue issue){
        return commentManager.getComments(issue);
    }

    public static int getTicketAge(MutableIssue issue) {
        Timestamp resolution = issue.getResolutionDate();
        if (resolution == null) // unresolved
            return (int) ((new Date().getTime() - issue.getCreated().getTime()) / 86400000);
        else // resolved
            return (int) ((resolution.getTime() - issue.getCreated().getTime()) / 86400000);
    }

}
