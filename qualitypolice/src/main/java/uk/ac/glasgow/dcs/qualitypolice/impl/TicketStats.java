package uk.ac.glasgow.dcs.qualitypolice.impl;

import com.atlassian.jira.issue.MutableIssue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class takes a MutableIssue as an argument
 * and produces an object, containing all metric values as public fields.
 */
public class TicketStats {
    public String reporter;
    public int wordCount;
    public int comments = 0;
    public double readability;
    public double volatility = 0;

    public TicketStats(MutableIssue issue) {
        reporter = issue.getReporter().getUsername();

        String text = issue.getDescription();
        if (text == null) {
            readability = 206.835;
            return;
        }
        // count sentences
        int sentenceCount = text.split("(?<=[a-z][!?.])\\s+").length;

        // count words
        List<String> listOfWords = new ArrayList<String>();
        Matcher m = Pattern.compile("\\w+").matcher(text);
        while (m.find()) listOfWords.add(m.group());
        wordCount = listOfWords.size();

        // count syllables
        int syllables = 0;
        for (String word : listOfWords) {
            m = Pattern.compile("[aeiouy]+").matcher(word);
            while (m.find()) syllables++;
        }
        readability = 206.835 - (1.015 * ((double) wordCount / (double) sentenceCount))
                - (84.6 * ((double) syllables / (double) wordCount));

        comments = getNumberComments(issue);

        volatility = (double) getNumberChanges(issue);
        int age = TicketUtils.getTicketAge(issue);
        if (age != 0) volatility /= (double) age;
    }

    private int getNumberComments(MutableIssue issue) {
        return TicketUtils.getTicketComments(issue).size();
    }

    private int getNumberChanges(MutableIssue issue){
        return TicketUtils.getTicketChanges(issue).size();
    }
}