package uk.ac.glasgow.dcs.qualitypolice.rest;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import uk.ac.glasgow.dcs.qualitypolice.impl.TicketStats;
import uk.ac.glasgow.dcs.qualitypolice.impl.TicketUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/stats")
public class TicketStatsResource {
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTicketStats(@QueryParam("project") String project,
                                   @QueryParam("user") String user,
                                   @QueryParam("incResolved") boolean incResolved,
                                   @QueryParam("incCurrent") boolean incCurrent) {
        List<MutableIssue> allIssues = TicketUtils.getTicketsFromQuery(project, user, incResolved, incCurrent);
        try {
            JSONObject json = new JSONObject();
            for (MutableIssue issue : allIssues) {
                TicketStats ticketStats = new TicketStats(issue);
                JSONObject statsObj = new JSONObject();
                statsObj.put("reporter", ticketStats.reporter);
                statsObj.put("wordCount", ticketStats.wordCount);
                statsObj.put("readability", ticketStats.readability);
                statsObj.put("comments", ticketStats.comments);
                statsObj.put("volatility", ticketStats.volatility);
                json.put(issue.getKey(), statsObj);
            }
            return Response.ok(json.toString(), MediaType.APPLICATION_JSON).build();
        } catch (Exception ignored) {
        }
        return null;
    }

}
