project = "";
user = "";

back = function(){};
var backlink = document.getElementById("back");
backlink.onclick = function(){back();};

function drawMainScreen(){
    back = function(){};
    backlink.style.display = "none";

    d3.select("svg").selectAll("*").remove();
    var svg = d3.select("svg"),
                margin = {top: 5, right: 5, bottom: 5, left: 5},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

    svg.append("text")
                .style("cursor", "default")
                .style("text-anchor", "left")
                .style("font-size", "20pt")
                .attr("transform", "translate(120,48)")
                .text("To proceed, choose a visualisation:");

    currentbutton = svg.append("g")
                .attr("transform", "translate(120,100)")
                .attr("id", "currentbutton")
                .on('click', drawTreeMap);
    pastbutton = svg.append("g")
                .attr("transform", "translate(460,100)")
                .attr("id", "pastbutton")
                .on('click', drawMetrics);

    currentbutton.append("rect")
                .attr("width", "300")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "currentrect")
                .attr("class", "ui-btn");
    pastbutton.append("rect")
                .attr("width", "300")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "pastrect")
                .attr("class", "ui-btn");

    currentbutton.append("text")
                .attr("transform", "translate(150,70)")
                .attr("class", "ui-btn-label")
                .text("Quality of current issues");
    pastbutton.append("text")
                .attr("transform", "translate(150,70)")
                .attr("class", "ui-btn-label")
                .text("Plot metrics of past issues");
}

function calcEstimate(d){
    wordCount = d.wordCount;
    readability = d.readability;
    comments = d.comments;
    volatility = d.volatility;
    terms = [1,
             Math.pow(0.01 + wordCount, -3),
             Math.pow(0.01 + wordCount, -2),
             Math.pow(0.01 + wordCount, -1),
             wordCount,
             Math.pow(0.01 + wordCount, 2),
             Math.exp(readability),
             readability,
             Math.pow(readability, 2),
             Math.pow(readability, 3),
             Math.pow(readability, 4),
             Math.pow(0.01 + comments, -1),
             comments,
             Math.pow(comments, 2),
             Math.pow(comments, 3),
             Math.pow(comments, 4),
             Math.pow(0.01 + volatility, -1),
             volatility,
             Math.pow(volatility, 2),
             Math.pow(volatility, 3),
             Math.pow(volatility, 4)];
    COEFFICIENTS = [ -7.61092476e+01,
                     -1.82089549e+01,
                     -2.13704609e+01,
                      6.50731740e+01,
                      8.30727333e-03,
                     -1.34384209e-07,
                      2.74094315e-83,
                      2.04823600e-02,
                      6.79510764e-07,
                      5.24636680e-12,
                      1.08262442e-17,
                     -2.01148238e-01,
                      8.06357310e+00,
                     -9.88706162e-02,
                      4.73108642e-04,
                     -6.44794726e-07,
                      1.59085173e+01,
                      1.75848855e+01,
                     -1.15048973e+00,
                      1.10004454e-02,
                     -2.30928153e-05]
    retval = 0;
    for (var i in terms){
        retval += terms[i] * COEFFICIENTS[i];
    }
    return retval;
}

function drawTreeMap(){
    back = drawMainScreen;
    backlink.style.display = "initial";

    d3.select("svg").selectAll("*").remove();
    var svg = d3.select("svg"),
                margin = {top: 5, right: 5, bottom: 5, left: 5},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

    var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d) {
            return "<b>"+(d.data.key || "all")+"</b>";
        })
    svg.call(tip);

    var treemap = d3.treemap()
                      .size([width, height])
                      .paddingOuter(4)
                      .paddingTop(19)
                      .paddingInner(4)
                      .round(true);

    var nest = d3.nest()
                .key(function(d) {return d.key.substring(0, d.key.indexOf('-')); })
                .key(function(d) {return d.value.reporter; })
                .rollup(function(leaves) { return leaves.length; });

    d3.json(BaseUrl + '/rest/ticket/1.0/stats/?project=&user=&incResolved=false&incCurrent=true', function(json) {
        data = [];
        for (ticket in json){
            data.push({key: ticket, value: json[ticket]});
        }

        estimatesForProjects = d3.nest()
                                   .key(function(d) {return d.key.substring(0, d.key.indexOf('-')); })
                                   .key(function(d) {return d.value.reporter; })
                                   .rollup(function(leaves) {return d3.mean(leaves, function(d){return calcEstimate(d.value)}); })
                                   .entries(data);

        var estimatesHierarchical = d3.hierarchy({values: estimatesForProjects}, function(d) { return d.values; })
                                     .eachAfter(function(node) {
                                                      var mean = +node.data.value || 0,
                                                          children = node.children,
                                                          i = children && children.length,
                                                          count = i;
                                                      while (--i >= 0) mean += children[i].value;
                                                      if (count > 0) mean /= count;
                                                      node.value = mean;
                                                  })
                                     .descendants();

        listOfEstimates = estimatesHierarchical.map(function(d){return d.value;})
        minestimate = Math.min.apply(null, listOfEstimates);
        maxestimate = Math.max.apply(null, listOfEstimates);


        domain = [minestimate,
                  6/7 * minestimate + 1/7 * maxestimate,
                  5/7 * minestimate + 2/7 * maxestimate,
                  4/7 * minestimate + 3/7 * maxestimate,
                  3/7 * minestimate + 4/7 * maxestimate,
                  2/7 * minestimate + 5/7 * maxestimate,
                  1/7 * minestimate + 6/7 * maxestimate,
                  maxestimate]
        var colourScale = d3.scaleLinear()
            .domain(domain)
            .range(["#28892c","#90eb9d","#ffff8c","#f9d057","#f29e2e","#e76818","#d7191c"])
            .interpolate(d3.interpolateHcl);

        var root = d3.hierarchy({values: nest.entries(data)}, function(d) { return d.values; })
              .sum(function(d) { return d.value; });

        treemap(root);

        var cell = g
            .selectAll(".node")
            .data(root.descendants())
            .enter().append("g")
              .attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; });

        index = 0;

        cell.append("rect")
              .attr("class", "node")
              .attr("id", function(d) {
                                switch (d.depth) {
                                    case 0: return "root"
                                    case 1: return d.data.key;
                                    default: return d.data.key + "-" + d.parent.data.key;
                                }
                         })
              .attr("title", function(d) { return d.data.key; })
              .attr("width", function(d) { return d.x1 - d.x0 + "px"; })
              .attr("height", function(d) { return d.y1 - d.y0 + "px"; })
              .style("fill-opacity", 1)
              .style("fill", function(d) {
                                 elem = estimatesHierarchical[index++];
                                 value = elem.value;
                                 return colourScale(value);
                             })
              .on('mouseover', tip.show)
              .on('mouseleave', tip.hide)
              .on('click', function(d,i){
                                tip.hide();
                                switch (d.depth) {
                                     case 2:
                                         user = d.data.key;
                                         project = d.parent.data.key;
                                         break;
                                     case 1:
                                         project = d.data.key;
                                         user = "";
                                         break;
                                     default: return;
                                }
                                drawPredictiveVisualisation();
                           });

        cell.append("clipPath")
              .attr("id", function(d) {
                                  switch (d.depth) {
                                      case 0: return "clip-root"
                                      case 1: return "clip-" + d.data.key;
                                      default: return "clip-" + d.data.key + "-" + d.parent.data.key;
                                  }
                           })
            .append("use")
              .attr("xlink:href", function(d) {
                                        switch (d.depth) {
                                            case 0: return "#root"
                                            case 1: return "#" + d.data.key;
                                            default: return "#" + d.data.key + "-" + d.parent.data.key;
                                        }
                                 });

        cell.append('text')
                .attr("clip-path", function(d) {
                                           switch (d.depth) {
                                               case 0: return "url(#clip-root)"
                                               case 1: return "url(#clip-" + d.data.key + ")";
                                               default: return "url(#clip-" + d.data.key + "-" + d.parent.data.key + ")";
                                           }
                                    })
                .attr("x", function(d) { return 4; })
                .attr("y", function(d) { return 13; })
                .attr("class", function(d) {
                                    switch (d.depth) {
                                        case 0:
                                        case 1: return "labelproj";
                                        default: return "labeluser";
                                    }
                               })
                .text(function(d) { return d.data.key; });
    });
}

function drawPredictiveVisualisation(){
    back = drawTreeMap;
    backlink.style.display = "initial";

    d3.select("svg").selectAll("*").remove();

    var svg = d3.select("svg"),
                margin = {top: 20, right: 20, bottom: 30, left: 25},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

    var x = d3.scaleBand().rangeRound([0, width]).padding(0.05).align(0.1);
    var y = d3.scaleLinear().rangeRound([height, 0]);

    var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    function mouseoverbright(d) {
        d3.select(this).transition().duration(10).style("fill-opacity", 0.3);
    };
    function mouseoffdark(d) {
        d3.select(this).transition().duration(10).style("fill-opacity", 1);
    };

    tooltip = function(d){return d["issue"]};

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d) {
            return tooltip(d);
        })
    svg.call(tip);

    d3.json(BaseUrl + '/rest/ticket/1.0/age/?project=' + project + "&user=" + user +
            "&incResolved=false&incCurrent=true",
      function(data) {
        ages = Object.keys(data);
        highestColumn = d3.max(ages, function(d) { return data[d].length; });
        x.domain(ages);
        y.domain([0, highestColumn]);

        svg.append("text")
            .style("text-anchor", "middle")
            .style("font-size", "12pt")
            .attr("transform", "translate(" + width/2 + "," + 15 + ")")
            .text("Current issues by age" + (project ? (", project: " + project) : "")
                                   + (user ? (", user: " + user) : ""));

        g.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        g.append("text")
            .style("text-anchor", "middle")
            .attr("transform", "translate(" + width/2 + "," + (25 + height) + ")")
            .text("Age (days)");

        g.append("g")
            .attr("class", "axis axis--y")
            .call(d3.axisLeft(y).ticks(highestColumn));

        processedData = [];
        for (var age in ages){
            for (var issue in data[ages[age]]){
                processedData.push({age: ages[age], issue: data[ages[age]][issue], height: parseInt(issue)+1});
            }
        }

        d3.json(BaseUrl + '/rest/ticket/1.0/stats/?project=' + project + "&user=" + user +
                "&incResolved=false&incCurrent=true",
          function(stats) {
            tooltip = function(d) {
                    name = d["issue"];
                    readability = Math.round(stats[d["issue"]].readability);
                    wordCount = stats[d["issue"]].wordCount;
                    comments = stats[d["issue"]].comments;
                    volatility = Math.round(stats[d["issue"]].volatility * 10)/10;
                    return "<strong>" + name + "</strong>" +
                           "<br />Readability: " + readability +
                           "<br />Word count: " + wordCount +
                           "<br />Comments: " + comments +
                           "<br />Volatility: " + volatility;
                };

            listOfEstimates = Object.keys(stats).map(function (key) { return stats[key]; }).map(calcEstimate);
            minestimate = Math.min.apply(null, listOfEstimates);
            maxestimate = Math.max.apply(null, listOfEstimates);

            domain = [minestimate,
                      6/7 * minestimate + 1/7 * maxestimate,
                      5/7 * minestimate + 2/7 * maxestimate,
                      4/7 * minestimate + 3/7 * maxestimate,
                      3/7 * minestimate + 4/7 * maxestimate,
                      2/7 * minestimate + 5/7 * maxestimate,
                      1/7 * minestimate + 6/7 * maxestimate,
                      maxestimate]
            var colourScale = d3.scaleLinear()
                .domain(domain)
                .range(["#28892c","#90eb9d","#ffff8c","#f9d057","#f29e2e","#e76818","#d7191c"])
                .interpolate(d3.interpolateHcl);

            g.selectAll(".bar")
              .data(processedData)
              .enter().append("a")
                .attr("xlink:href", function(d) {return BaseUrl+"/browse/"+d["issue"];})
                .append("rect")
                .attr("class", "bar")
                .attr("stroke", "white")
                .attr("x", function(d) { return x(parseInt(d["age"])); })
                .attr("y", function(d) { return y(d["height"]); })
                .attr("width", x.bandwidth())
                .attr("height", function(d) {return height - y(1); })
                .style("fill", function(d) {return colourScale(calcEstimate(stats[d.issue]))})
                .on('mouseover', function(d){
                    tip.show(d);
                    d3.select(this).style("fill-opacity", 0.6);
                 })
                .on('mouseleave', function(d){
                    d3.select(this).style("fill-opacity", 1);
                    tip.hide(d);
                });
        });
    });
}

function drawMetrics(){
    back = drawMainScreen;
    backlink.style.display = "inherit";

    d3.select("svg").selectAll("*").remove();
    var svg = d3.select("svg"),
                margin = {top: 5, right: 5, bottom: 5, left: 5},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

    svg.append("text")
                .style("cursor", "default")
                .style("text-anchor", "middle")
                .style("font-size", "20pt")
                .attr("transform", "translate(440,48)")
                .text("Choose a metric to inspect:");

    wordcountbutton = svg.append("g")
                .attr("transform", "translate(60,100)")
                .attr("id", "wordcountbutton")
                .on('click', function(){drawPastVisualisation("wordCount")});
    readabilitybutton = svg.append("g")
                .attr("transform", "translate(260,100)")
                .attr("id", "readabilitybutton")
                .on('click', function(){drawPastVisualisation("readability")});
    commentsbutton = svg.append("g")
                .attr("transform", "translate(460,100)")
                .attr("id", "commentsbutton")
                .on('click', function(){drawPastVisualisation("comments")});
    volatilitybutton = svg.append("g")
                .attr("transform", "translate(660,100)")
                .attr("id", "volatilitybutton")
                .on('click', function(){drawPastVisualisation("volatility")});

    wordcountbutton.append("rect")
                .attr("width", "160")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "wordcountrect")
                .attr("class", "ui-btn");
    readabilitybutton.append("rect")
                .attr("width", "160")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "readabilityrect")
                .attr("class", "ui-btn");
    commentsbutton.append("rect")
                .attr("width", "160")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "commentsrect")
                .attr("class", "ui-btn");
    volatilitybutton.append("rect")
                .attr("width", "160")
                .attr("height", "130")
                .attr("rx", "10")
                .attr("ry", "10")
                .attr("id", "volatilityrect")
                .attr("class", "ui-btn");

    wordcountbutton.append("text")
                .attr("transform", "translate(80,70)")
                .attr("class", "ui-btn-label")
                .text("Word count");
    readabilitybutton.append("text")
                .attr("transform", "translate(80,70)")
                .attr("class", "ui-btn-label")
                .text("Readability");
    commentsbutton.append("text")
                .attr("transform", "translate(80,70)")
                .attr("class", "ui-btn-label")
                .text("No. of comments");
    volatilitybutton.append("text")
                .attr("transform", "translate(80,70)")
                .attr("class", "ui-btn-label")
                .text("Volatility");
}


function drawPastVisualisation(metric){
    back = drawMetrics;
    backlink.style.display = "initial";

    d3.select("svg").selectAll("*").remove();

    var svg = d3.select("svg"),
                margin = {top: 20, right: 20, bottom: 30, left: 35},
                width = +svg.attr("width") - margin.left - margin.right,
                height = +svg.attr("height") - margin.top - margin.bottom;

    var y = d3.scaleLinear().rangeRound([height, 0]);
    var x = d3.scaleLinear().rangeRound([0, width]);

    var g = svg.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    function mouseoverbright(d) {
        d3.select(this).transition().duration(10).style("fill-opacity", 0.3);
    };
    function mouseoffdark(d) {
        d3.select(this).transition().duration(10).style("fill-opacity", 1);
    };

    tooltip = function(d){return d["issue"]};

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d) {
            return tooltip(d);
        })
    svg.call(tip);

    d3.json(BaseUrl + "/rest/ticket/1.0/age/?project=&user=&incResolved=true&incCurrent=false",
      function(data) {
        ages = Object.keys(data);
        d3.json(BaseUrl + "/rest/ticket/1.0/stats/?project=&user=&incResolved=true&incCurrent=false",
          function(stats) {
            tooltip = function(d) {
                    name = d["issue"];
                    readability = Math.round(stats[d["issue"]].readability);
                    wordCount = stats[d["issue"]].wordCount;
                    comments = stats[d["issue"]].comments;
                    volatility = Math.round(stats[d["issue"]].volatility * 10)/10;
                    return "<strong>" + name + "</strong>" +
                           "<br />Readability: " + readability +
                           "<br />Word count: " + wordCount +
                           "<br />Comments: " + comments +
                           "<br />Volatility: " + volatility;
                };

            processedData = [];
            for (var age in ages){
                for (var issue in data[ages[age]]){
                    processedData.push({age: ages[age], issue: data[ages[age]][issue], metric: stats[data[ages[age]][issue]][metric]});
                }
            }

            metricvalues = [];
            for (var issue in stats){
                metricvalues.push(stats[issue][metric]);
            }
            console.log(data);
            y.domain([Math.min.apply(Math, ages),Math.max.apply(Math, ages)]);
            x.domain([Math.min.apply(Math, metricvalues),Math.max.apply(Math, metricvalues)]);

            svg.append("text")
                .style("text-anchor", "middle")
                .style("font-size", "12pt")
                .attr("transform", "translate(" + width/2 + "," + 15 + ")")
                .text("Issues by age at time of resolution by " + metric);

            g.append("g")
                .attr("class", "axis axis--x")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            g.append("text")
                .style("text-anchor", "middle")
                .attr("transform", "translate(" + width/2 + "," + (25 + height) + ")")
                .text(metric);

            g.append("g")
                .attr("class", "axis axis--y")
                .call(d3.axisLeft(y));

            listOfEstimates = Object.keys(stats).map(function (key) { return stats[key]; }).map(calcEstimate);
            minestimate = Math.min.apply(null, listOfEstimates);
            maxestimate = Math.max.apply(null, listOfEstimates);

            domain = [minestimate,
                      6/7 * minestimate + 1/7 * maxestimate,
                      5/7 * minestimate + 2/7 * maxestimate,
                      4/7 * minestimate + 3/7 * maxestimate,
                      3/7 * minestimate + 4/7 * maxestimate,
                      2/7 * minestimate + 5/7 * maxestimate,
                      1/7 * minestimate + 6/7 * maxestimate,
                      maxestimate]
            var colourScale = d3.scaleLinear()
                .domain(domain)
                .range(["#28892c","#90eb9d","#ffff8c","#f9d057","#f29e2e","#e76818","#d7191c"])
                .interpolate(d3.interpolateHcl);

            g.selectAll(".dot")
              .data(processedData)
              .enter().append("a")
                .attr("xlink:href", function(d) {return BaseUrl+"/browse/"+d["issue"];})
                .append("circle")
                .attr("class", "dot")
                .attr("r", 3.5)
                .attr("cx", function(d) { return x(parseFloat(d["metric"])); })
                .attr("cy", function(d) { return y(parseInt(d["age"])); })
                .style("fill", function(d) {return colourScale(calcEstimate(stats[d.issue]))})
                .on('mouseover', function(d){
                    tip.show(d);
                    d3.select(this).style("fill-opacity", 0.6);
                 })
                .on('mouseleave', function(d){
                    d3.select(this).style("fill-opacity", 1);
                    tip.hide(d);
                });
        });
    });
}

drawMainScreen();